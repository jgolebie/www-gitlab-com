---
layout: handbook-page-toc
title: "Education, Open Source and Startup programs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Workflow for our complimentary initiatives:
1. [GitLab for Education](/solutions/education/)
1. [GitLab Open Source Program](/solutions/open-source/program/)
1. [GitLab Startups Program](/solutions/startups/)

Managing these programs include monitoring of:
1. SFDC chatter tab - for mentions of `@Community Advocate` inside SFDC
1. Zendesk views: *Education*, *Open Source*, *E-mail*, *Suspended tickets* (for `File {FileExternalID2} has been signed at GitLab` notifications)
1. Slack channels: `#education-oss`
1. Email aliases: education@gitlab.com, opensource@gitlab.com, and startups@gitlab.com. 
    * Follow the [steps to set up the `education@gitlab.com` email alias in your inbox](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/#setting-up-aliases). If you do not have access please submit an [access request](/handbook/business-ops/it-ops-team/access-requests/#open-an-access-request). 

The priorities for the daily workflow are as follows:
1. Renewal applications should have the highest priority in the queue because their license may be close to expiring. 
1. If there has already been some communication with the applicant regarding the use case for a new license, verifying the use case for a renewal license, or if a quote has been returned with a signature, these tickets should be processed first. 
1. New applications should be left until all tickets in the process have been moved forward. 


## Workflow

```mermaid
graph TB
    c1-->a1
    a12-->c2
    c2-->a5
    a9-->c3
    c3-->a10
    a10-->s1
    s2-->c4
    c4-->s3
    s3-->c5
    
    subgraph SALES-SUPPORT
        s1(Approve Opportunity in <br/>  Salesforce)-->s2(Send quote to Zuora )
        s3("Send activation instructions via e-mail  <br/> (optionally also license key)")-->s4("Create Renewal opportunity (1 year) ")
    end

    subgraph CUSTOMER
        c1[Submit form]
        c2(Send use case data)
        c3(Sign quote)
        c4(Accept EULA)
        c5{Self-hosted?}
        c5--"Yes (Ultimate)"-->c6(Install license key)
        c5--"No (Gold)"-->c7(Authenticate groups)
    end

    subgraph COMMUNITY ADVOCATE
        a1("Zendesk ticket created. <br/> New lead OR updated Contact <br/> in Salesforce. <br/>Both automatic, no action required.")-->a2[Determine if institution qualifies or not <br/> based on EULA]
        a2--"YES (Institution qualifies)"-->a3(Check if there is an existing <br/> account or <br/> active sales opportunity)
        a3--"YES (Existing active opportunity)"-->a13(Chatter sales account owner <br/> for visibility. <br/> See details below.)
        a13--"Sales approves"-->a12
        a3--"YES (Existing Edu opportunity)"-->a14(Send email using the <br/> 'license already issued' template. <br/> See details below.)
        a3--"NO"-->a12(Send email using the <br/> 'collecting participant usage data' <br/> template)
        a2--"NO (Institution does not qualify)"-->a4(Send email via Zendesk using the <br/> appropriate 'entity rejection' template)        
        a4-->a11(Follow Edu to sales handoff <br/> process if a non-student)
        a5[Determine if use case is acceptable or not <br/> based on EULA]
        a5--"YES (Use case acceptable )"-->a7(New Opportunity in Salesforce)
        a5--"NO (Use case not-acceptable)"-->a6(Send email via Zendesk using the  <br/> 'use case rejection' template & <br/> follow sales handoff process)  
a7-->a8(New Quote in Salesforce )
a8-->a9(Send Quote with Sertifi )
a10(Send Opportunity for approval <br/>  in Salesforce)    
  end
```

### Tools

We use several different tools to manage these programs:
1. [Community Advocacy Zendesk](/handbook/marketing/community-relations/community-advocacy/#zendesk), particularly *Education*, *Open source*, *E-mail* and *Suspended tickets* views
1. [Salesforce](/handbook/marketing/community-relations/community-advocacy/#salesforce)
1. [Email aliases](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/index.html#setting-up-aliases) for education@gitlab.com, opensource@gitlab.com and startups@gitlab.com
1. [Customers portal](/handbook/marketing/community-relations/community-advocacy/#customers-portal)
1. [LicenseApp](/handbook/marketing/community-relations/community-advocacy/#licenseapp)

### Determine if the application is valid

#### Education program

1. The Customer will submit the application form. 
1. A Zendesk Ticket is automatically created and either a New Lead or the existing Contact is updated SFDC. 
1. View the applicant's form in Zendesk. 
     - The data from the form will be forwarded to the SFDC as LEAD/CONTACT object. 
     - If an applicant selects `Role` = `Student` an automated email from *Marketo* is sent informing them of available options to students & how to encourage the University to apply. Use the the ['License' macro in Zendesk](/handbook/marketing/community-relations/community-advocacy/tools/zendesk/#current-macro-stack-descriptions) to close the ticket. There are no further steps. 
1. Verify that the educational institution is an actual accredited, academic, non-profit institution by doing an online search and reviewing its website. If the institution does not meet the criteria in the terms, use the ['academic use case' rejection template](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#rejecting-applicants-based-on-academic-use-case) to notify the applicant.
1. Check SFDC to see if there is an existing account and/or opportunity for this Educational Institution.
     - If there is an existing account with an active sales opportunity, chatter the account owner in SFDC to let them know of the Edu application and make sure there are no conflicts with the issuing the license. Wait 2 days to hear back. If there is no conflict, proceed with issuing the EDU license.
     - Make sure that we didn't already issue an Education license to that institution. Go to SFDC and search by their email address domain or the institution name. If we already provided them with an Education license, send the ['Edu license already issued'](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#response-templates) email via the education@ email alias to let them know.
1. If we didn't already issue an Education license to them, send the ['Collecting participants usage data' email template](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#response-templates) email via the education@ email alias to get more information about their GitLab usage and to make sure they are aligned with our Education program terms. 
1. The applicant should respond to your email with appropriate details. 
     - The email will appear in Zendesk. Please use Zendesk to reply to the applicant from this step forward.
     - Verifying the use case may take several back and forth emails with the applicant. If you are unsure please reach out to the Education Program Manager or CA Education Expert for assistance via the #edu-oss slack channel. It is better to qualify the lead now than issue a liscense in error. Once you have verified from the applicant that the use case does meet the EULA proceed to [Processing approved applications](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#processing-approved-applications) below. 
1. If the use case doesn't qualify, change the lead status to unqualified and send the 'academic use case' rejection template via Zendesk. Then follow the Education to Sales Handoff process below. 
1. Use the ['License' macro in Zendesk](/handbook/marketing/community-relations/community-advocacy/tools/zendesk/#current-macro-stack-descriptions) and change the related ticket's status to *Solved*
  
#### Open Source program

- Once the OSS form is submitted, we should receive the link to related Merge Request (see the application steps in our [OSS program page](/solutions/open-source/program/)).
- Navigate to their OSS project and make sure that they are using an OSI approved license. Also, make sure that they followed the steps to add themselves to the [OSS projects list](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml).
- If these requirements are met, see [processing approved applications](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#processing-approved-applications). If not, communicate to the user using opensource@gitlab.com (see [these steps](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/#setting-up-aliases) for more details).

#### Startups program

- Make sure they were part of the current or two most recent [YCombinator batches](https://www.ycombinator.com/companies/).
- Search [Crunchbase](https://www.crunchbase.com) by startup name to check if they raised less than $3M in revenue.

### Education to Sales Handoff Process 
If an applicant to the Education Program does not meet the terms, the account should be transferred over to the appropriate sales team by following these steps:
1. Identify the appropriate sales region and vertical of the entity here. 
    - If the entity is a public University in the US, the vertical is Public Sector. 
    - If the entity is a private University, the vertical is generally MidMarket.
    - Some regions may not have a Public Sector Vertical, in this case, use MidMarket.
    - Identify the appropriate SDR for the Region/Vertical.
1. Reassign the lead to the appropriate SDR for the Region/Vertical in SFDC. 
1. Chatter the SDR in SFDC to let them know the lead has been reassigned and there is no current action item.
    - If the lead responds to the offer in the 'academic use case' rejection template for an introduction to an SDR, then chatter the SDR to set up a call. 
1. Any relevant emails regarding the use case or the scope of the opportunity should be tracked within SFDC in the account's activity history. Follow [these steps](https://about.gitlab.com/handbook/customer-success/using-salesforce-within-customer-success/#tracking-emails-within-salesforce) to forward any relevant email correspondance to SFDC. 
    -  Note that if the person you are contacting is not yet a contact associated with an opportunity in SFDC you will need to search for the email and them manually associate in SFDC.

### Processing approved applications

- There are extra constraints and [rules of engagement](/handbook/sales/public-sector/#federal-government-rules-of-engagement) for customers whose email address ends in `mail.mil` or `.gov`
- Make sure to hand them over to the [US Public Sector team](/handbook/business-ops/#public-sector) for a review
- This is extremely important because they probably aren't qualified for this program even if it looks like they are. We could also end up in a legal violation of the US law if treated carelessly.

#### Step 1: Create an opportunity

1. Make sure that the *Billing Address* is entered correctly
1. If you're working with a **Lead** object, click on *Convert* (attach to the existing Account if possible). If you are working with an existing **Contact** click on *New Opportunity*.
1. The *Opportunity name* should follow this format: *CompanyName-NumberOfUsers Product Program [w/ Support]* (e.g. `ABC University-100 Ultimate EDU` or `ABC University-100 Ultimate YCStartups w/ Support`)
1. Turn off the *Reminder*
1. Set the *Initial Source* to Education/OSS (only when working with an existing Contact)
1. *Type* - New Business
1. *Close date* - Today
1. *Stage* - '6-Awaiting Signature'
1. Set the *Amount* to $0.00 if they didn't purchase support or the annual amount of the support price if they did

#### Step 2: Create a new Quote

1. If prompted, choose *New Billing Account*
1. *Quote Template* - `UNLIMITED ORDER FORM V3.2 w_ EDU ADDENDUM` / `UNLIMITED ORDER FORM V3.2 w_ OSS ADDENDUM` / `NEW STARTUP YC $0`
1. *Sold to* and *Bill to Contacts* are the Primary Contact
1. *Start Date* - Today
1. Turn off *Auto Renew* and click *Next*
1. *Add base product* - `Ultimate - 1 Year (EDU or OSS)` / `Ultimate - 1 Year (Y Combinator)`) or `Gold Plan - 1 Year (EDU or OSS)` / `Gold Plan - 1 Year (Y Combinator)` with or without support and click *Save*
1. *Quantity* - enter the number of seats and click *Submit*

#### Step 3: Send the quote to the customer

1. Navigate to the created Quote and click *Generate PDF*
1. Send the Quote to the customer by clicking on *Sertifi eSign* button. Use an adequate [email template](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#response-templates) and click *Next*. Select the newly created PDF and verify it is rendered properly with *Preview*.
1. Notify the customer that we processed their application and sent the quote (via the Zendesk ticket used for verifying their use case)
1. After the customer receives the quote, we will receive the `Delivery Receipt For File` email notification. Use the ['License' macro in Zendesk](/handbook/marketing/community-relations/community-advocacy/tools/zendesk/#current-macro-stack-descriptions) and change related ticket's status to *Solved*

#### Step 4: Submitting the Opportunity for the approval

1. When the customer signs the quote, we will receive the notification that will end up in the *Suspended tickets* view in Zendesk and will look like this: `File {FileExternalID2} has been signed at GitLab`. Use that ticket to find the related Opportunity in SFDC, update the *Close date* to today and click on *Submit for Approval*. Note - do not change the stage, only Deal Desk and Billing have permission to do that. 
1. Delete the related *Suspended Ticket* from Zendesk
1. Once approved, sales-support will send the quote to Zuora, and the click-through EULA will be automatically sent. After it is accepted, the license key will be sent (for Ultimate), or instructions for authenticating a group (for Gold). Also, a renewal opportunity will be created.  

### Renewal process
Note: All *Education*, *Open Source* and Startups renewals should be handled by Community Advocates. Please ping us on the [#education-oss](https://gitlab.slack.com/messages/education-oss) Slack channel if you have any questions. If the Opportunity is assigned to someone other than a community advocate and it is time to start the renewal process, please reassign the renewal opportunity to *Community Advocate*, but leave the related Account assigned to you.

#### Step 1: Update the renewal Opportunity

1. Find the adequate *Renewal Opportunity* and rename it using this format: *CompanyName-NumberOfUsers Product Program [w/ Support] Renewal RenewalDate* (e.g.*CompanyName-100 Gold EDU Renewal 01/19*). The date is the begining of the renewal year. If you can't find the appropriate renewal opportunity chatter *@sales-support*.
1. Make sure that the *Opportunity Type* is set to Renewal, set the *Close date* to today and *Stage* to '6-Awaiting Signature'. Also, double-check that the *Amount* is correct and click Save.

#### Step 2: Create the new renewal Quote

1. Click on *New quote*
1. Select appropriate *existing billing account*, select *renew existing subscription for this billing account* and find the Opportunity that you want to renew under *Subscription name*. Then click *Next*. 
1. Update *Sold to* and *Bill to Contact* on the *Provide Quote, Account and Subscription Terms* details page if needed. Everything else should stay the same. Click *Next*
1. Update the number of seats and/or attached product if needed and click *Submit*

#### Step 3: Final steps

- [Send the quote to the customer](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#step-3-send-the-quote-to-the-customer) and [submit the opportunity for approval](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#step-4-submitting-the-opportunity-for-the-approval) as described in the [Processing approved applications](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/#processing-approved-applications) section.
- The renewed license will become active only after the original expires (even if the renewal quote is approved before that).

### Creating an amendment for the existing subscriptions (adding more seats)

1. Navigate to the original (Closed Won) **Opportunity** and click *New Add On Opportunity*
   - *Opportunity name* - `CompanyName-Add [Quantity] [Product] EDU` (e.g. Oxford University- Add 25 Ultimate EDU)
   - Update: *Initial Source* to EDU/OSS, *Close date* to Today and *Stage* to '00-Pre Opportunity'
1. Select newly created **Opportunity** and create a new **Quote**:
   - Update: *Select Billing Account* to *existing billing account* and *Choose Quote Type* to *amend existing subscription for this billing account* and click *Next*
   - Click *Next* again (you can change signer if needed)
   - Click on *+ Add Products*, select Ultimate or Gold and click *Save*
   - Adjust the *Quantity* to however NEW users they want to add
   - Find where the old product purchase is listed, click downward arrow and click `Remove` and then `Submit`
1. Generate pdf or Word document and proceed as usual

Once the the quote is singed and approved, the LicenseApp will provision a new key for the increased number of users

## Best practices

### How to structure your projects

We drafted a set of best practices for overcoming challenges that you might encounter as a result of our decision to offer only one Education license per institution. There are also a couple of points clarifying who can participate in this offer and under what conditions.
- Only students and faculty directly involved in teaching students can access this subscription.
- This license can only be used during studies, so graduate students that aren't taking courses aren't allowed to use the instance.
- Student research that is part of their educational program and professor non-profit research with students participating is allowed.
- Seats are generic and are not specific to a user, as GitLab does not use a named license model. If a user doesn't need access anymore, it can be removed or blocked in order to free the seat. This seat can then be used by another user.
- Although we offer only one license key for the self-hosted solutions, it can be installed on multiple independent instances. Meaning that you can run two separate servers with the same license key. The only caveat is that currently there isn't an easy way to calculate the total number of consumed seats across all instances.
- You can manage the visibility of your projects with GitLab groups. A member of the parent group automatically has access to all descendants. GitLab doesn't support having the subgroup be more restrictive than its parent. However, being a part of a subgroup does not grant you access to the parent group. The best way to organize your work is to make everyone a member of their respective subgroup having only admins in the organizational (top-level) group.
- Please also see our [licensing and subscription FAQ](/pricing/licensing-faq/) section for more details.

### Public Sector exception

- There are extra constraints and [rules of engagement](/handbook/sales/public-sector/engaging-public-sector.html) for customers whose email address ends in `mail.mil` or `.gov`
- Make sure to hand them over to the [US Public Sector team](/handbook/business-ops/#public-sector) for a review
- This is extremely important because they probably aren't qualified for this program even if it looks like they are. We could also end up in a legal violation of the US law if treated carelessly.

### EULA modification requests

- At this time we are not contemplating modifications to the EULA.
- If we already approved EULA changes last year and the university wants to renew under the same terms, we can agree to that.

### Refund process

- Send an email to ar@gitlab.com with a link to the appropriate **Opportunity** and ask them to provide a refund request (they will create a refund opportunity)
- Communicate to the customer that they should see that payment reflect back to their records within 5-7 business days
- Proceed with new quotes/opportunities only when the refund process is done

### Processing tax exemption certificates

1. Attach these documents in the notes and attachments section of the opportunity
1. Enter the tax certificate ID in the Zuora quote. This should automatically remove the tax from the quote
1. CC sales-support for them to approve

### Adding Credit Card details

- The customer cannot pay directly by credit card. Rather, they need to add their credit card details on [customers.gitlab.com](https://customers.gitlab.com/).
- To do so, they need to follow these instructions:
  - Log into your account at [GitLab Subscription Manager](https://customers.gitlab.com/customers/sign_in)
  - Click on *Payment Methods* at the top of the page
  - Click on *Add new payment method*
  - Select *Credit Card* as the type
  - Enter the details and submit
  - Please send an e-mail to ar@gitlab.com once the credit card has been added and we will process the payment.
  
### Resolving users over license issue

If users used more seats during the previous year than they paid for, this error will appear:
- `During the year before this license started, this GitLab installation had X active users, exceeding this licenses limit of Y by Z users. Please upload a license for at least X users or contact sales at renewals@gitlab.com`
To resolve this issue, follow these steps:
  1. Go to the [LicenseApp](https://license.gitlab.com/licenses/) and find the particular license that needs to be changed
  1. Click on *Duplicate license*
  1. *Users count* - the number of seats they requested
  1. *Trueup count* - the maximum number of users during the previous year
  1. Click on *Create license* and the updated license will automatically be sent to the user


### Other

- [Naming conventions](/handbook/business-ops/#named-account-ownership)
  - Lead status: *Qualifying* - Advocate reached out to the user and asked for additional information
  - Lead status: *Unqualified* - User is not qualified for that program
- When you need to reassign a SFDC object from *Sales Admin* to *Community Advocate*, just click *Change* next to the object owner field and select *Community Advocate*
- **Case** objects: click *Close Case*, change Status to *Closed*, Case Reason to *Other* and leave an internal comment with actions you took to resolve it
- You can review the signed version of the quote by navigating to the *Opportunity* and selecting object under *Sertifi EContracts*. The link to the signed document ("Signed Link") should be under *EDoucments* subsection.
- Merging duplicated accounts in Salesforce: Make sure the domains and the account names are the same and reach out to Francis
- Payment options: ONLY when the Quote is approved and the license key delivered, our billing department will send an invoice where users can choose to pay via credit card, wire transfer or PO
- Handle one-license-per-institution rule on a case-by-case basis. We should first try to work with the institution so that they can manage their master account from a central department. Only if that does not work, we can discuss whether we can issue multiple licenses. 
- If you ever get an error while updating **Opportunity Status** from *00-Pre Opportunity* to *6-Awaiting Signature*, make sure to initially change it from *00-Pre Opportunity* to *0-Pending Acceptance* and then you'll be able to update it to *6-Awaiting Signature*.
- Resolving `Please obtain the necessary approvals before generating document(s)` error encountered while generating .pdf or Word file:
  1. Update *Submitter Comments* field on the quote object
  1. Click on *Submit for Approval* on the quote object
- Resolving `Insert failed. First exception on row 0; first error: INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY, insufficient access rights on cross-reference id...` error encountered while trying to send generated quote through Sertifi:
  1. Make sure that you are owner of the related Opportunity and Account objects. That should resolve the issue.

## Email templates

Below are email templates that can be used for communicating with Education program participants.

### Rejecting Students

```markdown
Hello,

Thanks for your interest in GitLab and applying to our Education Program!

We are so excited that you would like to learn about GitLab. Unfortunately, we are unable to accept your application because you do not meet our [eligibility requirements](https://about.gitlab.com/terms/#edu-oss).

Your application indicates that you are currently a student. Our GitLab Education Program only offers a centralized license to the University via a campus faculty or staff member. Please encourage a University representative to apply [here](https://about.gitlab.com/solutions/education/).

In the meantime, please check out our [free subscription](https://about.gitlab.com/pricing/#gitlab-com) for GitLab.com or a [free download](https://about.gitlab.com/pricing/#self-managed) of our core self-managed offering. You can also apply for a [30-day trial](https://gitlab.com/-/trials/new) if you’d like to try out some more advanced features.

We encourage you to check out all of our content at [GitLab Learning Tracks](https://about.gitlab.com/training/) to get started on your GitLab Journey.

Best regards,

The Education Program Team
```

### Rejecting applicants based on academic use case
```markdown
Hello,

Thank you for the additional details regarding your proposed use of GitLab. Unfortunately, the use case you described does not fall within the end-user license agreement for our Education Program. As specified in the agreement, the Education License granted includes access and/or use by IT professionals employed or contracted by Customer, solely for the purposes of providing administrative support to the Students and/or Faculty using the software in a classroom or research setting directly at non-profit Universities. 

All activities that fall outside of the purposes of teaching, learning, and research and/or activities conducted at a for profit University fit within a general separate academic license for IT professional. We are happy to connect you with a sales representative within your region to discuss our academic license options. 

Would you like us to connect you with our sales team? 

Thanks again,

The Education Program Team
```



### Collecting participants' usage data

```markdown
Hello, 

Thanks for your interest in GitLab and applying to our Education Program! 

In order to qualify for a free Educational Program License, we need to verify that your use case meets the requirements of our [End User License Agreement](https://about.gitlab.com/terms/#edu-oss). Note that University infrastructure operations and information technology operations do not fall within the stated terms of the Education Program. See our [FAQ section](https://about.gitlab.com/solutions/education/#FAQ) for additional details. 

Also, note that we issue only one license per campus. We recommend coordinating with other departments and requesting the total number of seats needed at this time. The primary contact will be able to request additional seats if more are needed at a future date. 

Lastly, once we receive verification of the use case and number of seats we will send a quote with the End User License Agreement attached. Only signatures by faculty or staff with proper signing authority on the behalf of the University will be accepted. If you do not meet these requirements, please include the contact information of the authorized representative. 

In order to proceed, please reply to this email with verification of the use case and any adjustments needed to the number of seats or primary contact information. 

Once we receive the above information, we will process your request and return a renewal quote for signature. Please allow a minimum of 10 business days for return. 

Please email us at education@gitlab.com with any questions. 

Best regards, 

The Education Program Team
```

### Notifying that the Education license has already been issued

```markdown
Hello, 

Thank you for applying for the GitLab Education program! 

Our records indicate that {Department Name*} already has been issued an Education license at the {University Name}. At this time, we are only able to issue one license per University. 

We encourage you to reach out to {Department Name*} to coordinate an email request for additional seats under the existing license to accommodate your use case. Please note that the additional seats must also meet the [End User License Agreement](https://about.gitlab.com/terms/#edu-oss). Note that University infrastructure operations and information technology operations do not fall within the stated terms of the license agreement. If your use case does not meet the terms, we are happy to connect you with our sales team. 

Please feel free to reach out to us with additional questions. 

Best regards, 

The Education Program Team
```
*Avoid using contact's name for privacy reasons.

### Requesting renewal information

```markdown
Hello, 

Thank you for being a valued GitLab Education Program participant! 

We noticed that your original Education License is set to expire soon. We hope you choose to renew and would like to assist you in the process. 

In order to renew, please reply directly to this email with the following information: 
Your original license is for {insert license type}. You are able to request more seats at this time if needed. Please indicate the number of seats for the renewal.  
Please verify the use case for the license. Specifically, we need verification that the use meets the [End User License Agreement](https://about.gitlab.com/terms/#edu-oss). Note that University infrastructure operations and information technology operations do not fall within the stated terms of the Education Program. See the [FAQ here](https://about.gitlab.com/solutions/education/#FAQ). 
Please include the full name, email address, and phone number of the primary contact who will be signing the renewal quote. Only signatures by faculty or staff with proper signing authority on the behalf of the University will be accepted.

Once we receive the above information, we will process your request and return a renewal quote for signature. Please allow a minimum of 2 business days for return. 

Please email us at education@gitlab.com with any questions. 

Best regards, 

The Education Program Team
```

### Instructions for authenticating Gold groups

```markdown
Information for GitLab.com Gold Upgrades: Groups must follow these instructions to authenticate.

Your subscription has been uploaded and you may follow these instructions to authenticate your groups:

1. Please visit https://customers.gitlab.com/customers/password/new to reset your account password
2. After logging in, please access the "Subscriptions" menu
3. You'll be able to click on "Edit" over a subscription
4. You'll be redirected to GitLab.com for OAuth login
5. At this point, you need to make sure you're logging in using the account you want to license on GitLab.com
6. Please select the Group you want to license then click onto "Update"

Please let me know if you have any questions, always happy to help.

Regards,
YOUR_NAME
```

### Sending a quote to the customer through Sertifi

```markdown
Hello,

We’re excited to tell you that your application for the GitLab (EDU or OSS) program has been approved and processed.

This is the quote that you need to sign. After you sign, you’ll receive the End-User License Agreement (EULA) that you need to accept via email. Then, we'll either send your license key via email (for Ultimate), or further instructions on how to authenticate your groups (for Gold).

If you'd like to help us promote this program, we would really appreciate if you sent a tweet using the #movingtogitlab hashtag!

Regards,
YOUR_NAME
```

## Automation

- The process in the customer portal should be updated, since most of the steps can be automated.
- See [this issue](https://gitlab.com/gitlab-com/marketing/community-relations/education-program/general/issues/3) in our education program issue tracker for more details.
